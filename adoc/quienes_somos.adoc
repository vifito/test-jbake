= Quienes somos
include::includes/header.adoc[]
:jbake-type: page
:jbake-tags: 
:jbake-status: draft

image::images/banner.png[link=index.html]

== Colaboradores

=== H. R. Fabara C (Comunicación)
Humanista Tecnológico Pragmático, Sincretista y Divulgador Tecnológico.

Diseñador y Comunicador Web Multimedia

* Usuario y Partidario Moderado de: https://mintcompartimosyayudamos.wordpress.com/2019/05/12/que-es-linux-mint[Linux Mint], Software Libre y de Código Abierto
* Director y Asesor en https://mintcompartimosyayudamos.wordpress.com[mint • compartimos y ayudamos]
* Difusor Social en https://t.me/tecnologia_hoy[Tecnología Hoy: GNU / Linux • Android • Raspberry Pi]

Web personal: https://hrfabarac.wordpress.com

Perfil de Mastodon: https://mastodon.social/@hrfabarac

=== Miguel Pujante (Comunicación)
Sherpa, instigador y agitador en social media. Posicionamiento orgánico y sordo de un pie.

Desarrollador y Comunicador Web Multimedia sobre WordPress

* Partidario Moderado de: GNU/Linux, Software Libre y de Código Abierto
* Usuario de Ubuntu-Mate y Raspberry Pi OS
* Director y Asesor en desmárcate ¡YA! y Calafellonfire.
* Redactor/corrector en Canalbaixpenedes.cat
* Asocial orgánico  

Web personal: https://mpujante.eu

Perfil en Twitter: https://twitter.com/miguelthepooh

=== José GDF (Audio y Vídeo)
Usuario de Linux Mint, músico y podcaster.

Fundador del grupo https://t.me/HomeStudioLibre[@HomeStudioLibre]

Blog personal: https://www.josegdf.net/

@JoseGDF en https://twitter.com/JoseGDF[Twitter] y en Telegram


== link:ponente-01.html[Moderadores]
