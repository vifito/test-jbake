title=Construcción del sitio con jbake
type=page
tags=
status=published
withtitle=true
~~~~~~

## Construír la web empleando Docker

```bash
# Descargar la imagen de docker con jbake
docker pull vifito/jbake

# Para desarrollar en local, se actualicen automáticamente los cambios
# y los vemos en http://localhost:8820
docker run -it --name jbake --network=host \
  -v `pwd`:/var/jbake/ --rm vifito/jbake:latest \
  jbake -b -s . public

# Entrando en el contenedor podemos hacer uso de jbake
docker run --name jbake -it -v `pwd`:/var/jbake/ --rm vifito/jbake:latest bash
# Ejecutar dentro del contenedor y construír en el directorio public
jbake . public
```

## Construír con jbake

```bash
# Instalación de sdkman
curl -s "https://get.sdkman.io" | bash

# Inicializar sdkman
source "$HOME/.sdkman/bin/sdkman-init.sh"

# Instalar java (ver otras versiones de java con: sdk list java)
sdk install java 11.0.8.j9-adpt

# Instalar jbake
sdk install jbake 2.6.5

# Ejecutar en el directorio del proyecto con jbake
#  -b para construír (bake)
#  -s para servir los contenidos en localhost:8820
jbake -b -s . public
```

## Construír en gitlab

Ver el fichero `.gitlab-ci.yml`.
