
if (document.querySelector('#conversejs')) {
  var cfgConverse = {
      allow_logout: true,
      authentication: 'anonymous',
      assets_path: '/dist/',
      auto_login: true,
      auto_join_rooms: [
          '24h24l@conference.localhost',
      ],
      bosh_service_url: 'https://desenvolvemento.software/ekohria4bequahLu/',
      debug: true,
      jid: 'localhost', 
      notify_all_room_messages: [
          '24h24l@conference.localhost',
      ],
      prebind: false,
      singleton: true,
      show_client_info: false,
      show_controlbox_by_default: false,
      locales_url: "/dist/locales/es-LC_MESSAGES-converse-po.js",
      view_mode: 'embedded',
      theme: 'concord'
  };

  converse.initialize(cfgConverse); 
}