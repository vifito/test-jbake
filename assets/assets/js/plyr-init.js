var playerConfig = {  // Localisation
  i18n: {
    restart: 'Reiniciar',
    rewind: 'Rebobinar {seektime}s',
    play: 'Play',
    pause: 'Pausa',
    fastForward: 'Avanzar {seektime}s',
    seek: 'Saltar',
    seekLabel: '{currentTime} de {duration}',
    played: 'Played',
    buffered: 'Almacenado',
    currentTime: 'Current time',
    duration: 'Duración',
    volume: 'Volumen',
    mute: 'Silencio',
    unmute: 'Unmute',
    download: 'Descargar',
    frameTitle: 'Reproductor para {title}',
    settings: 'Ajustes',
    pip: 'PIP',
    menuBack: 'Ir al menú previo',
    speed: 'Velocidad',
    normal: 'Normal',
    quality: 'Calidad',
    loop: 'Bucle',
    start: 'Inicio',
    end: 'Fin',
    all: 'Todo',
    reset: 'Reset',
    disabled: 'Deshabilitado',
    enabled: 'Habilitado',
    advertisement: 'Ad',
    qualityBadge: {
      2160: '4K',
      1440: 'HD',
      1080: 'HD',
      720: 'HD',
      576: 'SD',
      480: 'SD',
    }
  },
  autoplay: false,
  settings: ['quality', 'speed'],
  controls : [
    //'rewind',
    'play',
    'progress',
    'current-time',
    'duration',
    'mute',
    'volume',
    'settings',
    'pip',
    'airplay',
    'download'
  ]
};

var player = null;

function createPlayer(cfg) {
  if (player != null) {
    player.destroy();
  }
  player = new Plyr('.player audio', cfg);
  document.querySelector('.player img').addEventListener("click", function() {
    player.togglePlay();
  });
}

if (!window.matchMedia("only screen and (max-device-width: 480px)").matches) {
  createPlayer(playerConfig);
} else {
  var cfg = playerConfig;
  cfg.controls = ['play','progress','current-time','volume'];
  createPlayer(cfg);
}
