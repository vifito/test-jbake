FROM alpine:latest

LABEL version="2.6.5" maintainer="vifito <vifito@gmail.com>"

RUN apk -U upgrade && \
    apk add curl unzip zip bash libstdc++ ca-certificates java-cacerts openjdk11-jre && \
    rm -rf /var/cache/apk/* && \
    mkdir -p /var/jbake 

ENV JBAKE_VERSION=2.6.5 \
    PATH=$PATH:/root/.sdkman/candidates/jbake/current/bin:/usr/lib/jvm/default-jvm/bin \
    JAVA_HOME=/usr/lib/jvm/default-jvm

SHELL ["bash", "-c"]

WORKDIR /var/jbake

RUN curl -s "https://get.sdkman.io" | bash && \
    source "$HOME/.sdkman/bin/sdkman-init.sh" && \
    sdk install jbake $JBAKE_VERSION

EXPOSE 8820